import 'package:final_exam/common/welcome.dart';
import 'package:final_exam/page/universepage/universe_page.dart';
import 'package:final_exam/widget/appbar.dart';
import 'package:flutter/material.dart';
import 'package:unicorndial/unicorndial.dart';

import 'infopage/info_page.dart';
import 'notepage/note_page.dart';
import 'solarpage/solar_page.dart';
import 'unitpage/unit_page.dart';
import 'videopage/video_tutorial.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var childButtons = List<UnicornButton>();
    childButtons.add(UnicornButton(
        hasLabel: true,
        labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
        labelHasShadow: false,
        labelText: "Video tutorial",
        labelColor: Colors.white,
        currentButton: FloatingActionButton(
          heroTag: "videopage",
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
          mini: true,
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => VideoPlayerScreen(),
                ));
          },
        )));
    // childButtons.add(UnicornButton(
    //     hasLabel: true,
    //     labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
    //     labelHasShadow: false,
    //     labelText: "Thiên hà",
    //     labelColor: Colors.white,
    //     currentButton: FloatingActionButton(
    //       heroTag: "universepage",
    //       backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
    //       mini: true,
    //       child: Icon(
    //         Icons.explore,
    //         color: Colors.white,
    //       ),
    //       onPressed: () {
    //         Navigator.push(
    //             context,
    //             MaterialPageRoute(
    //               builder: (context) => UniversePage(),
    //             ));
    //       },
    //     )));
    childButtons.add(UnicornButton(
        hasLabel: true,
        labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
        labelHasShadow: false,
        labelText: "Hệ mặt trời",
        labelColor: Colors.white,
        currentButton: FloatingActionButton(
          heroTag: "solarpage",
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
          mini: true,
          child: Icon(
            Icons.wb_sunny,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SolarPage(),
              ),
            );
          },
        )));
    childButtons.add(UnicornButton(
        hasLabel: true,
        labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
        labelHasShadow: false,
        labelText: "Chuyển đổi đơn vị",
        labelColor: Colors.white,
        currentButton: FloatingActionButton(
          heroTag: "Unitpage",
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
          mini: true,
          child: Icon(
            Icons.receipt,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => UnitPage(),
              ),
            );
          },
        )));
    // childButtons.add(UnicornButton(
    //     hasLabel: true,
    //     labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
    //     labelHasShadow: false,
    //     labelText: "Chú thích",
    //     labelColor: Colors.white,
    //     currentButton: FloatingActionButton(
    //       heroTag: "notepage",
    //       backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
    //       mini: true,
    //       child: Icon(
    //         Icons.note_add,
    //         color: Colors.white,
    //       ),
    //       onPressed: () {
    //         Navigator.push(
    //             context,
    //             MaterialPageRoute(
    //               builder: (context) => NotePage(),
    //             ));
    //       },
    //     )));
    childButtons.add(UnicornButton(
        hasLabel: true,
        labelBackgroundColor: Color.fromRGBO(255, 255, 255, 0),
        labelHasShadow: false,
        labelText: "Thông tin",
        labelColor: Colors.white,
        currentButton: FloatingActionButton(
          heroTag: "infopage",
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
          mini: true,
          child: Icon(
            Icons.info,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InfoPage(),
                ));
          },
        )));
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              'assets/background/1.jpg',
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          GradientAppBar("Menu"),
          Welcome()
        ],
      ),
      floatingActionButton: UnicornDialer(
        animationDuration: 360,
          hasBackground: true,
          parentButtonBackground: Color.fromRGBO(255, 255, 255, 0.5),
          orientation: UnicornOrientation.VERTICAL,
          parentButton: Icon(
            Icons.menu,
            color: Colors.white,
          ),
          childButtons: childButtons),
    );
  }
}