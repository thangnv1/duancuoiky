import 'package:final_exam/widget/appbar.dart';
import 'package:flutter/material.dart';

import 'universe_page_body.dart';

class UniversePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Center(
          child: Image.asset(
            'assets/background/4.jpg',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
        ),
        Center(
          child: new Column(
            children: <Widget>[
              new GradientAppBar("Thiên hà Milky Way"),
              new UniversePageBody(),
            ],
          ),
        ),
      ]),
    );
  }
}