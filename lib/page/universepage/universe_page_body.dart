import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

class UniversePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: RaisedButton(
          onPressed: () {
            showDialog(
                context: context,
                builder: (_) => NetworkGiffyDialog(
                      onlyOkButton: true,
                      image: Image.network(
                        "https://media.giphy.com/media/35TCsltnD9BpOZ6Gv8/giphy.gif",
                        fit: BoxFit.fill,
                      ),
                      title: Text('Chú ý',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 22.0, fontWeight: FontWeight.w600)),
                      description: Text(
                        'Tính năng đang trong quá trình hoàn thiện, vui lòng đợi cập nhật',
                        style: TextStyle(color: Colors.redAccent),
                        textAlign: TextAlign.center,
                      ),
                      onOkButtonPressed: () {
                        Navigator.pop(context);
                      },
                    ));
          },
        ),
      ),
    );
  }
}
