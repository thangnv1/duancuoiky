import 'package:final_exam/common/ui/info_card_widget.dart';
import 'package:final_exam/model/info_model/info.dart';
import 'package:final_exam/widget/appbar.dart';
import 'package:final_exam/widget/card_item.dart';
import 'package:final_exam/widget/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InfoPage extends StatelessWidget {
  InfoPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Center(
          child: Image.asset(
            'assets/background/7.jpg',
            fit: BoxFit.fill,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        GradientAppBar("Thông tin"),
        Container(
          child: MenuPager(
            children: Aliments.aliments
                .map(
                  (aliment) => Page(
                    icon: aliment.bottomImage,
                    child: CardItem(
                      child: AlimentWidget(
                        aliment: aliment,
                        theme: aliment.background,
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ]),
    );
  }
}
