import 'package:final_exam/page/solarpage/solar_page_body.dart';
import 'package:final_exam/widget/appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SolarPage extends StatelessWidget {
  SolarPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Center(
          child: Image.asset(
            'assets/background/3.jpg',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
        ),
        Center(
          child: new Column(
            children: <Widget>[
              new GradientAppBar("Hệ Mặt Trời"),
              new SolarPageBody(),
            ],
          ),
        ),
      ]),
    );
  }
}
