import 'package:final_exam/common/ui/planet_summary.dart';
import 'package:final_exam/model/solar_model/planets.dart';
import 'package:flutter/material.dart';

class SolarPageBody extends StatelessWidget{
@override
  Widget build(BuildContext context) {
    return new Expanded(
      child: new CustomScrollView(
        scrollDirection: Axis.vertical,
        shrinkWrap: false,
        slivers: <Widget>[
          new SliverPadding(
            padding: const EdgeInsets.symmetric(vertical: 24.0),
            sliver: new SliverList(
              delegate: new SliverChildBuilderDelegate(
                (context, index) => new PlanetSummary(planets[index]),
                childCount: planets.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
