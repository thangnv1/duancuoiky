import 'package:final_exam/common/text_style.dart';
import 'package:final_exam/common/ui/planet_summary.dart';
import 'package:final_exam/model/solar_model/planets.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final Planets planet;
  DetailPage(this.planet);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: _getBackground(context),
          ),
          _getContent(),
          _getToolbar(context),
        ],
      ),
    );
  }

  _getBackground(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return new Container(
      child: new Image.network(
        planet.picture,
        fit: BoxFit.cover,
        width: size.width,
        height: size.height,
      ),
    );
  }

  _getContent() {
    final _overviewTitle = "MÔ TẢ".toUpperCase();
    return new Container(
      child: new ListView(
        padding: new EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
        children: <Widget>[
          new PlanetSummary(
            planet,
            horizontal: false,
          ),
          new Container(
            padding: new EdgeInsets.symmetric(horizontal: 32.0),
            child: new Column(
              children: <Widget>[
                new Text(
                  _overviewTitle,
                  style: Style.headerTextStyle,
                  textAlign: TextAlign.center,
                ),
                new Text(
                  planet.description,
                  style: Style.commonTextStyle,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _getToolbar(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: new BackButton(color: Colors.white),
    );
  }
}
