import 'package:final_exam/widget/appbar.dart';
import 'package:flutter/material.dart';

class UnitPage extends StatefulWidget {
  @override
  _UnitPageState createState() => _UnitPageState();
}

class _UnitPageState extends State<UnitPage> {
  final TextEditingController _weight = new TextEditingController();
  double _result = 0.0;
  String _finaltext = "";
  int val1 = 0;
  handleval(int value) {
    setState(
      () {
        val1 = value;
        switch (value) {
          case 0:
            _result = clcweight(
              _weight.text,
              0.38,
            );
            _finaltext =
                "Cân nặng tại sao Thủy = ${_result.toStringAsFixed(1)}";
            break;
          case 1:
            _result = clcweight(_weight.text, 0.91);
            _finaltext = "Cân nặng tại sao Kim = ${_result.toStringAsFixed(1)}";
            break;
          case 2:
            _result = clcweight(_weight.text, 1.0);
            _finaltext =
                "Cân nặng tại Trái Đất = ${_result.toStringAsFixed(1)}";
            break;
          case 3:
            _result = clcweight(_weight.text, 0.38);
            _finaltext = "Cân nặng tại sao Hỏa = ${_result.toStringAsFixed(1)}";
            break;
          case 4:
            _result = clcweight(_weight.text, 2.34);
            _finaltext = "Cân nặng tại Sao Mộc = ${_result.toStringAsFixed(1)}";
            break;
          case 5:
            _result = clcweight(_weight.text, 1.06);
            _finaltext = "Cân nặng tại sao Thổ = ${_result.toStringAsFixed(1)}";
            break;
          case 6:
            _result = clcweight(_weight.text, 0.92);
            _finaltext =
                "Cân nặng tại sao Thiên Vương = ${_result.toStringAsFixed(1)}";
            break;
          case 7:
            _result = clcweight(_weight.text, 1.19);
            _finaltext =
                "Cân nặng tại sao Hải Vương = ${_result.toStringAsFixed(1)}";
            break;
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              "assets/background/8.jpg",
              fit: BoxFit.fill,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
          ),
          GradientAppBar("Tính cân nặng"),
          ListView(
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(top: 150),
                alignment: Alignment.center,
                child: new Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Container(
                        height: 25,
                        child: Center(
                          child: new Text(
                            _weight.text.isEmpty
                                ? "Nhập cân nặng"
                                : _finaltext + " kg",
                            style: TextStyle(
                                color: Colors.yellowAccent,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w200,
                                fontSize: 20),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 75.0),
                      child: new TextField(
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                        cursorColor: Colors.white,
                        decoration: new InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Cân nặng của bạn ?',
                            labelStyle: TextStyle(
                              color: Colors.white70,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600,
                            ),
                            hintText: 'kg',
                            hintStyle: TextStyle(
                              color: Colors.white70,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600,
                            )),
                        controller: _weight,
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 70.0),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    "assets/solar_system/mercury.png",
                                    width: 31,
                                    height: 31,
                                  ),
                                ),
                                new Radio<int>(
                                  value: 0,
                                  groupValue: val1,
                                  onChanged: handleval,
                                ),
                                new Text(
                                  "Mer",
                                  style: TextStyle(
                                    color: Colors.white70,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/solar_system/venus.png",
                                width: 31,
                                height: 31,
                              ),
                              new Radio<int>(
                                value: 1,
                                groupValue: val1,
                                onChanged: handleval,
                              ),
                              new Text(
                                "Ven",
                                style: TextStyle(
                                  color: Colors.white70,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 70.0),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    "assets/earth.png",
                                    width: 31,
                                    height: 31,
                                  ),
                                ),
                                new Radio<int>(
                                  value: 2,
                                  groupValue: val1,
                                  onChanged: handleval,
                                ),
                                new Text(
                                  "Ear ",
                                  style: TextStyle(
                                    color: Colors.white70,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/solar_system/mars.png",
                                width: 31,
                                height: 31,
                              ),
                              new Radio<int>(
                                value: 3,
                                groupValue: val1,
                                onChanged: handleval,
                              ),
                              new Text(
                                "Mar",
                                style: TextStyle(
                                  color: Colors.white70,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 70.0),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    "assets/solar_system/jupiter.png",
                                    width: 31,
                                    height: 31,
                                  ),
                                ),
                                new Radio<int>(
                                  value: 4,
                                  groupValue: val1,
                                  onChanged: handleval,
                                ),
                                new Text(
                                  "Jup ",
                                  style: TextStyle(
                                    color: Colors.white70,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/solar_system/saturn.png",
                                width: 31,
                                height: 31,
                              ),
                              new Radio<int>(
                                value: 5,
                                groupValue: val1,
                                onChanged: handleval,
                              ),
                              new Text(
                                "Sat",
                                style: TextStyle(
                                  color: Colors.white70,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 70.0),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    "assets/solar_system/uranus.png",
                                    width: 31,
                                    height: 31,
                                  ),
                                ),
                                new Radio<int>(
                                  value: 6,
                                  groupValue: val1,
                                  onChanged: handleval,
                                ),
                                new Text(
                                  "Ura ",
                                  style: TextStyle(
                                    color: Colors.white70,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/solar_system/neptune.png",
                                width: 31,
                                height: 31,
                              ),
                              new Radio<int>(
                                value: 7,
                                groupValue: val1,
                                onChanged: handleval,
                              ),
                              new Text(
                                "Nep",
                                style: TextStyle(
                                  color: Colors.white70,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  clcweight(String weight, double grav) {
    if (int.parse(weight).toString().isNotEmpty && int.parse(weight) > 0) {
      return int.parse(weight) * grav;
    } else {
      print("error");
    }
  }
}
