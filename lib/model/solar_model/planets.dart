class Planets {
  final String id,
      name,
      location,
      distance,
      gravity,
      weight,
      satellite,
      description,
      image,
      picture;

  const Planets(
      {this.id,
      this.name,
      this.location,
      this.distance,
      this.gravity,
      this.weight,
      this.satellite,
      this.description,
      this.image,
      this.picture});
}

List<Planets> planets = [
  const Planets(
    id: "1",
    name: "Mặt Trời",
    location: "Dải Milky Way",
    distance: "1.000 AU",
    gravity: "27,400 g",
    weight: "1.989E30 kg",
    satellite: "525",
    description: "-  Đường kính trung bình: 1,392 ×106 km.\n"
        "-   Diện tích bề mặt: 6,0877E12 km².\n"
        "-   Vận tốc thoát ly: 617,54 km/s.\n"
        "-   Nhiệt độ bề mặt: 5.780 K.\n"
        "-   Nhiệt độ nhật hoa: 5 triệu K.\n"
        "-   Độ sáng (LS): 3,846×1026 W.\n"
        "    Mặt Trời là hành tinh ở trung tâm Hệ Mặt Trời, chiếm khoảng 99,86% khối lượng của Hệ Mặt Trời.\n"
        "    Thành phần của Mặt Trời gồm hydro (khoảng 74% khối lượng, hay 92% thể tích), heli (khoảng 24% khối lượng, 7% thể tích), và một lượng nhỏ các nguyên tố khác, gồm sắt, nickel, oxy, silic, lưu huỳnh, magiê, carbon, neon, canxi, và crom.\n"
        "    Mặt Trời có hạng quang phổ G2V. G2 có nghĩa nó có nhiệt độ bề mặt xấp xỉ 5.778 K (5.505 °C) khiến nó có màu trắng, và thường có màu vàng khi nhìn từ bề mặt Trái Đất bởi sự tán xạ khí quyển. Chính sự tán xạ này của ánh sáng ở giới hạn cuối màu xanh của quang phổ khiến bầu trời có màu xanh.\n"
        "    Trong vùng từ 0,25 tới khoảng 0,7 bán kính Mặt Trời, vật liệu Mặt Trời đủ nóng và đặc đủ để bức xạ nhiệt chuyển được nhiệt độ từ trong lõi ra ngoài.",
    image: "assets/solar_system/sun.png",
    picture:
        "https://images.wallpaperscraft.com/image/sun_buildings_outlines_139412_1600x1200.jpg",
  ),
  const Planets(
      id: "2",
      name: "Sao Thủy",
      location: "Dải Milky Way",
      distance: "0.613 AU",
      gravity: "0.380 g",
      weight: "3.285E23 kg",
      satellite: "0",
      description: "-  Tốc độ vũ trụ cấp 1: 47,87 km/s.\n"
          "-  Tốc độ vũ trụ cấp 2: 4,25 km/s.\n"
          "-  Bán kính trung bình: 2.439,7 ± 1,0 km.\n"
          "-  Diện tích bề mặt: 7,48E7 km2.\n"
          "-  Chu kỳ tự quay: 58,646 ngày.\n"
          "   Sao Thủy hay Thủy Tinh là hành tinh nhỏ nhất và gần Mặt Trời nhất trong tám hành tinh thuộc Hệ Mặt Trời, với chu kỳ quỹ đạo bằng 88 ngày Trái Đất.\n"
          "   Sao Thủy là một trong 4 hành tinh kiểu Trái Đất trong Hệ Mặt Trời, và là hành tinh cấu tạo bằng đá giống Trái Đất.\n"
          "   Bề mặt Sao Thủy có rất nhiều hố to nhỏ và lởm chởm như bề mặt Mặt Trăng, gồm các đồng bằng và hố va chạm lớn, cho thấy nó đã trải qua một thời gian yên tĩnh địa chất hàng tỷ năm.\n"
          "   Các hố va chạm trên Sao Thủy có đường kính từ những hốc nhỏ cho đến những hố nhiều vành rộng hàng trăm kilômét. Chúng đều ở trạng thái bị phong hóa dần, từ những hố tỏa tia tương đối mới cho đến những hố tồn tại từ lâu chỉ còn lại dấu vết mờ.\n"
          "   Nhiệt độ bề mặt của Sao Thủy dao động từ 100 K (-173⁰C) đến 700 K (427⁰C) do sự thiếu vắng bầu khí quyển (khí quyển cực kỳ mỏng) và gradient nhiệt độ biến đổi mạnh giữa xích đạo và các cực.\n"
          "   Mặc dù có kích thước nhỏ và vận tốc quay quanh trục chậm, Sao Thủy có một từ trường đáng chú ý và dường như phân bố trên toàn bộ hành tinh này. Theo những đo lường từ tàu Mariner 10, từ trường của nó mạnh khoảng 1,1% so với từ trường Trái Đất.\n"
          "Quỹ đạo của Sao Thủy là một hình elip rất hẹp, độ dài của bán trục chính là 70 triệu km trong khi bán trục nhỏ chỉ có 46 triệu km. Vận tốc quỹ đạo của Sao Thủy rất cao vì ảnh hưởng trọng lực của Mặt Trời.\n"
          "   Trong chiêm tinh học phương Tây, Sao Thủy là hành tinh trị vì cung Song Tử và Thất Nữ, hay ảnh hưởng về mặt chiêm tinh học của hành tinh này là lớn nhất trong hai cung hoàng đạo này.",
      image: "assets/solar_system/mercury.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-5.jpg"),
  const Planets(
      id: "3",
      name: "Sao Kim",
      location: "Dải Milky Way",
      distance: "0.276 AU",
      gravity: "0.980 g",
      weight: '4.867E24 kg',
      satellite: "0",
      description:
          "Sao Kim (cách Mặt Trời khoảng 0,7 AU) có kích cỡ khá gần với kích thước Trái Đất (với khối lượng bằng 0,815 lần khối lượng Trái Đất) và đặc điểm cấu tạo giống Trái Đất, nó có một lớp phủ silicat dày bao quanh một lõi sắt. Sao Kim có một bầu khí quyển dày và có những chứng cứ cho thấy hành tinh này còn sự hoạt động của địa chất bên trong nó. Tuy nhiên, Sao Kim khô hơn Trái Đất rất nhiều và mật độ bầu khí quyển của nó gấp 90 lần mật độ bầu khí quyển của Trái Đất. Sao Kim không có vệ tinh tự nhiên. Nó là hành tinh nóng nhất trong hệ Mặt Trời với nhiệt độ của bầu khí quyển trên 400 °C, nguyên nhân chủ yếu là do hiệu ứng nhà kính của bầu khí quyển. Không có dấu hiệu cụ thể về hoạt động địa chất gần đây được phát hiện trên Sao Kim (một lý do là nó có bầu khí quyển quá dày), mặt khác hành tinh này không có từ trường để ngăn chặn sự suy giảm đáng kể của bầu khí quyển, và điều này gợi ra rằng bầu khí quyển của nó thường xuyên được bổ sung bởi các vụ phun trào núi lửa.",
      image: "assets/solar_system/venus.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-6.jpg"),
  const Planets(
      id: "4",
      name: "Trái Đất",
      location: "Dải Milky Way",
      distance: "0.000 AU",
      gravity: "1.000 g",
      weight: '5.972E24 kg',
      satellite: "1",
      description:
          "Trái Đất (cách Mặt Trời 1 AU) là hành tinh lớn nhất và có mật độ lớn nhất trong số các hành tinh vòng trong, cũng là hành tinh duy nhất mà chúng ta biết còn có các hoạt động địa chất gần đây, và là hành tinh duy nhất trong vũ trụ được biết đến là nơi có sự sống tồn tại. Trái Đất cũng là hành tinh đá duy nhất có thủy quyển lỏng, và cũng là hành tinh duy nhất nơi quá trình kiến tạo mảng đã được quan sát. Bầu khí quyển của Trái Đất cũng khác căn bản so với các hành tinh khác với thành phần phân tử ôxy tự do thiết yếu cho sự sống chiếm tới 21% trong bầu khí quyển.[46] Trái Đất có một vệ tinh tự nhiên là Mặt Trăng, nó là vệ tinh tự nhiên lớn nhất trong số các vệ tinh của các hành tinh đá trong hệ Mặt Trời.",
      image: "assets/solar_system/earth.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-7.jpg"),
  const Planets(
      id: "5",
      name: "Sao Hỏa",
      location: "Dải Milky Way",
      distance: "0.523 AU",
      gravity: "0.380 g",
      weight: '6.390E23 kg',
      satellite: "2",
      description:
          "Sao Hỏa (cách Mặt Trời khoảng 1,5 AU) có kích thước nhỏ hơn Trái Đất và Sao Kim (khối lượng bằng 0,107 lần khối lượng Trái Đất). Nó có một bầu khí quyển chứa chủ yếu là cacbon điôxít với áp suất khí quyển tại bề mặt bằng 6,1 millibar (gần bằng 0,6% áp suất khí quyển tại bề mặt của Trái Đất). Trên bề mặt hành tinh đỏ có những ngọn núi khổng lồ như Olympus Mons (cao nhất trong hệ Mặt Trời) và những rặng thung lũng như Valles Marineris, với những hoạt động địa chất có thể đã tồn tại cho đến cách đây 2 triệu năm về trước. Bề mặt của nó có màu đỏ do trong đất bề mặt có nhiều sắt oxit (gỉ). Sao Hỏa có hai Mặt Trăng rất nhỏ (Deimos và Phobos) được cho là các tiểu hành tinh bị Sao Hỏa bắt giữ. Sao Hỏa là hành tinh có cấu tạo gần giống Trái Đất nhất.",
      image: "assets/solar_system/mars.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-8.jpg"),
  const Planets(
      id: "6",
      name: "Sao Mộc",
      location: "Dải Milky Way",
      distance: "4.202 AU",
      gravity: "2.528 g",
      weight: '1.898E27 kg',
      satellite: "67",
      description:
          "Sao Mộc (khoảng cách đến Mặt Trời 5,2 AU), với khối lượng bằng 318 lần khối lượng Trái Đất và bằng 2,5 lần tổng khối lượng của 7 hành tinh còn lại trong Thái Dương Hệ. Mộc Tinh có thành phần chủ yếu hiđrô và heli. Nhiệt lượng khổng lồ từ bên trong Sao Mộc tạo ra một số đặc trưng bán vĩnh cửu trong bầu khí quyển của nó, như các dải mây và Vết đỏ lớn.Sao Mộc có 63 vệ tinh đã biết. Bốn vệ tinh lớn nhất, Ganymede, Callisto, Io, và Europa (các vệ tinh Galileo) có các đặc trưng tương tự như các hành tinh đá, như núi lửa và nhiệt lượng từ bên trong.[63] Ganymede, vệ tinh tự nhiên lớn nhất trong hệ Mặt Trời, có kích thước lớn hơn Sao Thủy.",
      image: "assets/solar_system/jupiter.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-9.jpg"),
  const Planets(
      id: "7",
      name: "Sao Thổ",
      location: "Dải Milky Way",
      distance: "8.512 AU",
      gravity: "1.065 g",
      weight: '5.683E26 kg',
      satellite: "~200",
      description:
          "Sao Thổ (khoảng cách đến Mặt Trời 9,5 AU), có đặc trưng khác biệt rõ rệt đó là hệ vành đai kích thước rất lớn, và những đặc điểm giống với Sao Mộc, như về thành phần bầu khí quyển và từ quyển. Mặc dù thể tích của Thổ Tinh bằng 60% thể tích của Mộc Tinh, nhưng khối lượng của nó chỉ bằng một phần ba so với Mộc Tinh, hay 95 lần khối lượng Trái Đất, khiến nó trở thành hành tinh có mật độ nhỏ nhất trong hệ Mặt Trời (nhỏ hơn cả mật độ của nước lỏng). Vành đai Sao Thổ chứa bụi cũng như các hạt băng và đá nhỏ.Sao Thổ có 62 vệ tinh tự nhiên được xác nhận; hai trong số đó, Titan và Enceladus, cho thấy có các dấu hiệu của hoạt động địa chất, mặc dù đó là các núi lửa băng. Titan, vệ tinh tự nhiên lớn thứ hai trong Thái Dương Hệ, cũng lớn hơn Sao Thủy và là vệ tinh duy nhất trong hệ Mặt Trời có tồn tại một bầu khí quyển đáng kể.",
      image: "assets/solar_system/saturn.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-10.jpg"),
  const Planets(
      id: "8",
      name: "Sao Thiên Vương",
      location: "Dải Milky Way",
      distance: "18.180 AU",
      gravity: "0.866 g",
      weight: '8.681E25 kg',
      satellite: "27",
      description:
          "Sao Thiên Vương (khoảng cách đến Mặt Trời 19,6 AU), khối lượng bằng 14 lần khối lượng Trái Đất, là hành tinh bên ngoài nhẹ nhất. Trục tự quay của nó có đặc trưng lạ thường duy nhất so với các hành tinh khác, độ nghiêng trục quay trên 90 độ so với mặt phẳng hoàng đạo. Thiên Vương Tinh có lõi lạnh hơn nhiều so với các hành tinh khí khổng lồ khác và nhiệt lượng bức xạ vào không gian cũng nhỏ. Sao Thiên Vương có 27 vệ tinh tự nhiên đã biết, lớn nhất theo thứ tự từ lớn đến nhỏ là Titania, Oberon, Umbriel, Ariel và Miranda.",
      image: "assets/solar_system/uranus.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-11.jpg"),
  const Planets(
      id: "9",
      name: "Sao Hải Vương",
      location: "Dải Milky Way",
      distance: "29.073 AU",
      gravity: "1.180 g",
      weight: '1.024E26 kg',
      satellite: "14",
      description:
          "Sao Hải Vương (khoảng cách đến Mặt Trời 30 AU), mặc dù kích cỡ hơi nhỏ hơn Sao Thiên Vương nhưng khối lượng của nó lại lớn hơn (bằng 17 lần khối lượng của Trái Đất) và do vậy khối lượng riêng lớn hơn. Nó cũng bức xạ nhiều nhiệt lượng hơn nhưng không lớn bằng của Sao Mộc hay Sao Thổ. Sao Hải Vương có 13 vệ tinh tự nhiên đã biết. Triton là vệ tinh lớn nhất vầ còn sự hoạt động địa chất với các mạch phun nitơ lỏng. Triton cũng là vệ tinh tự nhiên duy nhất có qũy đạo nghịch hành. Trên cùng quỹ đạo của Sao Hải Vương cũng có một số hành tinh vi hình (en:minor planet), gọi là các thiên thể Troia của Sao Hải Vương, chúng cộng hưởng quỹ đạo 1:1 với Sao Hải Vương.",
      image: "assets/solar_system/neptune.png",
      picture:
          "http://www.giaoduc.edu.vn/upload/images/2016/10/14/khoa-hoc-vu-tru-thu-tu-cua-8-hoac-9-hanh-tinh-trong-he-mat-troi-12.jpg"),
];
