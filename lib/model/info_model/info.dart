import 'package:flutter/material.dart';


class Aliment {
  final String name;
  final LinearGradient background;
  final String subtitle;
  final String image;
  final String bottomImage;
  final String studentID;

  Aliment({ this.name,
    this.background,
    this.subtitle,
    this.image,
    this.bottomImage,
    this.studentID,
  });
}

class Aliments {
  static List<Aliment> aliments = [
    Aliment(
        name: "Thắng",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFFFD8183), const Color(0xFFFB425A)],
        ),
        subtitle: "K62A3 MT&KHTT",
        image: "assets/info/author.svg",
        bottomImage: "assets/info/bottom.svg",
        studentID: "17000231",),
  ];
}
