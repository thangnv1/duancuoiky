import 'package:flutter/material.dart';

import 'page/home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.white,
          accentColor: Colors.white,
          dialogBackgroundColor: Color.fromRGBO(255, 255, 255, 0.2)),
      home: HomePage(),
    );
  }
}
