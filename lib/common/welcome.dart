import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class Welcome extends StatelessWidget {
  List<Widget> _textAnimationKit = [
    SizedBox(
      width: 350.0,
      child: TypewriterAnimatedTextKit(
        text: [
          "Xin chào ,",
          "Chào mừng đến với thế giới của tôi",
        ],
        textStyle: TextStyle(
            fontSize: 50.0,
            fontFamily: "TravelingTypewriter",
            color: Color.fromRGBO(255, 153, 0, 1)),
      ),
    ),
  ];
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: _textAnimationKit[_index],
    );
  }
}
